#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/GpuPreference.hpp>
#include <SFML/Main.hpp>
#include <SFML/Network.hpp>


#include "Player.h"
#include "Image.h"

using namespace std;


class Game
{
private:
	sf::RenderWindow window;
	sf::Event ev;
	sf::Texture texture;


	Player* player;
	void initWindow();
	void initPlayer();

public:

	Game();
	virtual ~Game();

	//Functions
	void updatePlayer();

	void update();
	void renderPlayer();
	void render();
	const sf::RenderWindow& getWindow() const;
};
