#include "Player.h"
#include <iostream>

Player::Player()
{
	this->initVariables();
	this->initTexture();
	this->initSprite();
	
}

Player::~Player()
{
}


void Player::initTexture()
{
	if (!this->playerTexture.loadFromFile("player_sheet.png"))
	{
		std::cout << "Error::Player::Could not load the player sprite sheet" << "\n";
	}
}

// try with another sprite model HERE!!!
void Player::initSprite()
{
	this->sprite.setTexture(this->playerTexture);
	this->sprite.setPosition(100.f, 400.f);
	textureSize = playerTexture.getSize();
	textureSize.x /= 10;
	textureSize.y /= 6;
	this->currentFrame = sf::IntRect(0, 0, textureSize.x, textureSize.y);
	this->sprite.setTextureRect(currentFrame);
	this->sprite.setScale(2.5f, 2.5f);
}



void Player::initVariables()
{
	this->moving = false;
}




void Player::updateMovement()
{
	this->moving = false;
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Left))
	{
		this->sprite.move(-5.f, 0.f);
		this->moving = true;
	}

	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Right))
	{
		this->sprite.move(5.f, 0.f);
		this->moving = true;
	}


	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Up))
	{
		this->sprite.move(0.f, -5.f);
		this->moving = true;
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Key::Down))
	{
		this->sprite.move(0.f, 5.f);
		this->moving = true;
	}
}

void Player::update()
{
	this->updateMovement();
	
}

void Player::render(sf::RenderTarget& target)
{
	target.draw(this->sprite);
}

