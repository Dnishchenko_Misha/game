 #include <iostream>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <exception>
#include <cmath>
#include <thread>

#include "Image.h"
#include "Game.h"
#include "Menu.h"

using namespace std;
using namespace cv;
using namespace sf;



int main()
{


    //srand(static_cast<unsigned>(time(0)));
    MyImage A(0, CAP_ANY);
    Game G;
    //Menu M(800, 600);
    //M.render();
    //A.ImageOpen(0, CAP_ANY);
    //A.ImageRead();
   
    while (G.getWindow().isOpen())
    {
        
        G.update();
        G.render();
        
        A.ImageRead();

    }
                
   // second.join();
    
    return 0;
   
}