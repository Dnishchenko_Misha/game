#pragma once
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/GpuPreference.hpp>
#include <SFML/Main.hpp>
#include <SFML/Network.hpp>

#include "Image.h"
using namespace std;

enum PLAYER_ANIMATIONS
{
	IDLE = 0,
	MOVE_LEFT,
	MOVE_RIGHT,
	JUMP,
	FALL

};

class Player:public MyImage
{
	

private:
	sf::Sprite sprite;
	sf::Texture playerTexture;
	bool moving;
	sf::Clock animationTimer;
	sf::Vector2u textureSize;

	//Core
	void initTexture();
	void initSprite();
	void initAnimations();
	void initVariables();

	//Animation
	sf::IntRect currentFrame;

public:
	Player();
	virtual ~Player();

	//Functions
	void updateAnimations();
	void updateMovement();
	void update();
	void render(sf::RenderTarget& target);


	//setters
	 sf::Sprite& Setsprite() { return sprite; }
};

