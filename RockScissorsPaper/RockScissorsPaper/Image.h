#pragma once

#include <iostream>
#include <opencv2/imgproc/types_c.h>
#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/videoio.hpp>
#include <opencv2/highgui.hpp>
#include <exception>
#include <cmath>
#include <filesystem>


#include <vector>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <SFML/Config.hpp>
#include <SFML/GpuPreference.hpp>
#include <SFML/Main.hpp>
#include <SFML/Network.hpp>

#include "controls.h"
using namespace std;
using namespace cv;

class MyImage
{
	
	private:
		cv::Mat frame;
		cv::VideoCapture cap;
		int ImDeviceID = 0;             // 0 = open default camera
		int ImApiID = cv::CAP_ANY;	//audetect default API

		int number_of_defects = 0;
		double hull_area;
		double area_ratio;
		double object_area;
		double angle_between_fingers;
		//int number_of_defects2;

		cv::Mat ROI;
		cv::Mat hsv_rectangle;
		cv::Mat hsv_mask;
		vector<Point> approx_curve;
		vector<Vec4i> defects;
		vector<Point> convex_hull;
		vector<int> defects_hull;

	public:
		MyImage() {};
		MyImage(int CamID, int ApiID);

		//setters and getters 
		void 	set_CamID(int CamID) { ImDeviceID = CamID; }
		void	set_ApiID(int ApiID) { ImApiID = ApiID; }
		void	SetROI();
		void	SetColor();

		//functions
		void		GameConrol();
		int		ImageOpen(int CamID, int ApiID);
		void	ImageRead();
};



	
	
	
