#include <iostream>
#include "Image.h"

#include "Game.h"
#include "controls.h"
#include<list>;

using namespace std;
using namespace cv;

MyImage::MyImage(int CamID, int ApiID)
    :
    ImDeviceID(CamID),
    ImApiID(ApiID)
{
    cap.open(CamID, ApiID);
}

int MyImage::ImageOpen(int CamID, int ApiID)
{
    if (!cap.isOpened())
    {
        cerr << "ERROR! Unable to open camera\n";
        return -1;
    }
}

void MyImage::ImageRead()
{
    
    if (!cap.isOpened()) 
    {
        cerr << "ERROR! Unable to open camera\n";
        
    }
        cap.read(frame);

        if (frame.empty()) 
        {
            cerr << "ERROR! blank frame grabbed\n";
           
        }

        cv::flip(frame, frame, 1);

        //set Region of Interest
        Mat ROI(frame, Rect(340, 100, 270, 270));
        cv::imshow("ROI", ROI);

        //color and blur changing
        cv::cvtColor(ROI, hsv_rectangle, COLOR_BGR2HSV);
        cv::inRange(hsv_rectangle, cv::Scalar(0, 20, 70), cv::Scalar(20, 255, 255), hsv_mask);
        cv::dilate(hsv_mask, hsv_mask, Mat(), Point(-1, -1), 4);
        cv::GaussianBlur(hsv_mask, hsv_mask, Size(5, 5), 100);
        //imshow("Mask", hsv_mask);


        //TODO CHANGE COORDINATES FOR RECTANGE
        // Top Left Coordinates
        Point p1(600, 200);
        // Bottom Right Coordinates
        Point p2(400, 400);

        int thickness = 5;

        //change coordinates
        rectangle(frame, p1, p2,
            Scalar(255, 0, 0),
            thickness, LINE_8);
        //imshow("Output", frame);

        // Press  ESC on keyboard to exit

        char c = (char)waitKey(25);

        if (c == 27)

         exit;

        number_of_defects = 0;

        vector<vector<Point> > get_contours;
        vector<Vec4i> hierarchy;

        //cv::findContours(hsv_mask, get_contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
       

        findContours(hsv_mask.clone(), get_contours, hierarchy, RETR_TREE, CV_CLOCKWISE, Point(0, 0));

        /**/
        if (get_contours.size() != 0)
        {
            double largest_area = 0;
            int largest_countour_index = 0;

            for (int i = 0; i < get_contours.size(); i++)
            {
                double a = cv::contourArea(get_contours.at(i));
                // cout<<a<<"\n";
                if (a > largest_area)
                {
                    largest_area = a;
                    largest_countour_index = i;
                    //  cout<<"New largest area:"<<i<<"--->"<<largest_area<<"\n\n\n\n";
                }

            }

            double epsilon;
            epsilon = 0.0005 * cv::arcLength(get_contours.at(largest_countour_index), true);

            //vector<Point> approx_curve;
            cv::approxPolyDP(get_contours[largest_countour_index], approx_curve, epsilon, true);


            // vector<Point> convex_hull;
            cv::convexHull(get_contours[largest_countour_index], convex_hull);

            //double hull_area;
            hull_area = cv::contourArea(convex_hull);

            // double object_area;
            object_area = cv::contourArea(get_contours[largest_countour_index]);

            double area_ratio;
            area_ratio = (hull_area - object_area) / object_area;

            //------------------------------------------------------------
            //vector<int> defects_hull;
            cv::convexHull(approx_curve, defects_hull, false, false);

            //vector<Vec4i> defects;
            cv:convexityDefects(approx_curve, defects_hull, defects);

            // int number_of_defects = 0;

            for (const Vec4i& v : defects)
            {
                int startidx = v[0];
                //                          cout<<startidx<<"-->"<<approx_curve[startidx]<<"\n";
                Point ptStart(approx_curve[startidx]);
                //                          cout<<ptStart.x<<ptStart.y;
                int endidx = v[1];
                Point ptEnd(approx_curve[endidx]);
                int faridx = v[2];
                Point ptFar(approx_curve[faridx]);
                float depth = v[3];
                
                //building triangle

                double ab, bc, ca;
                ab = sqrt(pow(ptEnd.x - ptStart.x, 2) + pow(ptEnd.y - ptStart.y, 2));
                bc = sqrt(pow(ptFar.x - ptStart.x, 2) + pow(ptFar.y - ptStart.y, 2));
                ca = sqrt(pow(ptEnd.x - ptFar.x, 2) + pow(ptEnd.y - ptFar.y, 2));

                double side, area;
                side = (ab + bc + ca) / 2;
                area = sqrt(side * (side - ab) * (side - bc) * (side - ca));

                //double angle_between_fingers;
                angle_between_fingers = acos((pow(bc, 2) + pow(ca, 2) - pow(ab, 2)) / (2 * (bc * ca)));
                cout << angle_between_fingers << "-->";
                angle_between_fingers = (angle_between_fingers * 180 * 7) / 20;//22


                if (angle_between_fingers <= 90 && depth > 30)//30-default|| angle_between_fingers >90 && angle_between_fingers <130&& depth > 3000)
                {
                    cout << "Defect Found";
                    number_of_defects++;
                    cv::circle(ROI, ptFar, 1, cv::Scalar(255, 0, 0));
                }
               
                cv::line(ROI, ptStart, ptEnd, cv::Scalar(0, 255, 0));

            }
            number_of_defects++;
          
            cout << number_of_defects << "\n";
         
            GameConrol();//Show number of fingers and control Player

        }

        //imshow("Mask", hsv_mask);
        cv::imshow("Live", frame);
        
}

void MyImage::SetROI()

{
    Mat ROI(frame, Rect(340, 100, 270, 270));
    cv::imshow("ROI", ROI);
}

void MyImage::SetColor()
{
    cv::cvtColor(ROI, hsv_rectangle, COLOR_BGR2HSV);
    cv::inRange(hsv_rectangle, cv::Scalar(0, 20, 70), cv::Scalar(20, 255, 255), hsv_mask);
    cv::dilate(hsv_mask, hsv_mask, Mat(), Point(-1, -1), 4);
    cv::GaussianBlur(hsv_mask, hsv_mask, Size(5, 5), 100);
    cv::imshow("Mask", hsv_mask);
}



void MyImage::GameConrol()
{
    if (number_of_defects == 1)
    {
        if (object_area < 2000)
        {
            cv::putText(frame, "Move hand inside the box", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 20, cv::Scalar(0, 0, 255));

            unPressAll();
        }

        else
        {
            if (area_ratio < 12)
            {
                cv::putText(frame, "0", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));

                unPressAll();
            }
          
            if (angle_between_fingers > 90 && angle_between_fingers < 120)
            {
                cv::putText(frame, "1", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));

                number_of_defects = 0;

                pressLeft();
            }

        }
       
    }
    
    else if (number_of_defects == 2)
    {

        cv::putText(frame, "2", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));
        
        pressUp();
    }
    else if (number_of_defects == 3)
    {
        if (area_ratio < 27)
        {
            cv::putText(frame, "3", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));

        }
       
    }
    else if (number_of_defects == 4) 
    {
        cv::putText(frame, "4", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));
        pressDown();
       
        //unPressDown();
    }
    
    else if (number_of_defects == 5) 
    {
         cv::putText(frame, "5", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));
        number_of_defects == 5;

         pressRight();
       
    }

    else
    {
        cv::putText(frame, "Reposition", Point(0, 50), cv::FONT_HERSHEY_SIMPLEX, 2, cv::Scalar(0, 0, 255));
        unPressAll();
        
    }
    
    
}
